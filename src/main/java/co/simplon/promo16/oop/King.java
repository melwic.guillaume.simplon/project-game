package co.simplon.promo16.oop;

import java.util.Scanner;


public class King {

    /**En paramètre, nous avons le pattern du regex et le message d'erreur, avec des les réponses aux 2 dernières questions.*/

    protected String error = "Cela ne répond pas à la question.";
    private String firstName;
    private String name;
    protected String pattern = "^[A-Za-z]+$";
    protected String guessChar;
    private char ans = 'è';
    private String guess1;
    private String guess2;

    public void intro(Scanner scanner) {

    /**Méthode d'introduction qui nous permet de stocker le prénom et le nom, qui nous servira plus tard.*/

        System.out.println(
                "🤵 : Bienvenue dans le royaume de Nolpmis, je suis le valet et vous vous trouvez actuellement devant le château du roi Avaj Nolpmis. Pour entrer et rencontrer le roi, veuillez nous donner votre prénom et nom. Quel est votre prénom ?");
        firstName = scanner.next();
        while (!firstName.matches(pattern) || firstName.length() < 2) {
            System.out.println(error);
            firstName = scanner.next();
        }
        System.out.println("🤵 : " + firstName + " ! C'est un beau prénom et quel est votre nom ?");

        name = scanner.next();
        while (!name.matches(pattern) || name.length() < 2) {
            System.out.println(error);
            name = scanner.next();
        }

        System.out.println("🤵 : " + firstName + " " + name
                + " ! Très bien, vous pouvez entrer mais vous devez d'abord rencontrer le prince Tig. Il est dans sa chambre, c'est tout droit, allez-y.");

        for (int i = 0; i < 5; i++) {
            if (i == 0 || i == 4) {
                System.out.println("|🧑");
            } else
                System.out.println("|*");
        }
        System.out.println("[Chambre du prince]");
    }

    public void surprise(Scanner scanner) {

        /**Méthode surprise avec une énigme avec un regex. De la part du valet, qui n'était pas prévu de base.*/

        System.out.println(
                "🤵 : Ne me dites pas que vous m'aviez oublié ! J'ai aussi une énigme pour vous, si vous trouvez la réponse, je vous laisse entrer : Je transforme une plante en une planète. Qui suis-je ?");
        boolean loop = true;
        while (loop) {
            guessChar = scanner.next().toLowerCase();
            if (guessChar.length() > 1 && guessChar.matches(pattern)) {
                System.out.println("🤵 : Vous n'êtes pas sur la bonne piste.");
                continue;
            } else if (guessChar.length() > 1 && !guessChar.matches(pattern)) {
                System.out.println(error);
                continue;
            } else if (guessChar.toLowerCase().charAt(0) != ans) {
                System.out.println("🤵 : Ce n'est pas la bonne réponse, recommencez.");
                continue;
            } else
                System.out.println(
                        "🤵 : Vous êtes très malin ! Je vous laisse donc entrer.");
            loop = false;
        }
        for (int i = 0; i < 5; i++) {
            if (i == 0 || i == 4) {
                System.out.println("|🧑");
            } else
                System.out.println("|*");
        }
        System.out.println("[Grande salle]");
    }

    public void end(Scanner scanner) {

        /**Méthode finale avec la dernière énigme et qui conclue le jeu.*/

        System.out.println("👑🤴 : Bonjour, je suis le roi Avaj, du royaume Nolpmis. Si tu es parvenu jusqu'ici, c'est que tu as trouvé les réponses aux énigmes précédentes. Je vais te donner mon énigme. Si tu trouves la réponse, je te donnerais une belle récompense. Mais ne rêve pas trop, tu n'auras la main d'aucun de mes enfants : Je veux que tu me proposes 2 objets qui te correspondent, "
                + firstName + " " + name + ". Quel est ton premier objet ?");
        boolean loop1 = true;
        while (loop1) {
            guess1 = scanner.next();
            if (guess1.toLowerCase().charAt(0) != firstName.toLowerCase().charAt(0)) {
                System.out
                        .println("👑🤴 : Je n'accepte pas cet objet, en tout cas pas en premier. Peut-être après. Propose-moi autre chose.");
                continue;
            } else if (!guess1.matches(pattern) || guess1.length() < 2) {
                System.out.println(error);
                continue;
            } else
                System.out.println("👑🤴 : C'est d'accord, j'accepte cet objet en premier. Maintenant je veux le deuxième objet.");
            loop1=!loop1;
        }
        boolean loop2=true;
        while (loop2){
            guess2 = scanner.next();
            if (guess2.toLowerCase().charAt(0) != name.toLowerCase().charAt(0)) {
                System.out
                        .println("👑🤴 : Je n'accepte pas cet objet. Propose-moi autre chose.");
                continue;
            } else if (!guess2.matches(pattern) || guess2.length() < 3) {
                System.out.println(error);
                continue;
            } else
                System.out.println("👑🤴 : "+guess1+" et "+guess2+"! Mes félicitations ! Je voulais 2 objets qui commence par les premières lettres de ton prénom et ton nom. Tu es très fort, tu as trouvé toutes les réponses aux énigmes. Comme récompense, tu peux t'installer dans mon royaume. Si tu souhaites démarrer une carrière dans n'importre quel domaine, vas-y. Tu auras tous les droits et autorisations pour réaliser tes projets. Et tout ça, à mes frais. Nous avons besoin de personnes intelligentes et malines comme toi dans le royaume pour qu'il se développe. Bravo et bienvenue dans mon royaume "+firstName+"!");
            loop2=!loop2;
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}