package co.simplon.promo16.oop;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Enigma extends King {

    /**Les paramètres ci-dessous sont les réponses aux questions, pour qu'on puisse les comparer aux entrées pour valider la réponse.*/

    private int guessInt;
    private int ans1 = 2;
    private char ans2 = 'n';
    private int ans3 = 33;

    public void enigme1(Scanner scanner) {

        /**Méthode d'une énigme avec un try-catch, qui nous permet d'entrer que des chiffres.*/

        System.out.println(
                "🤴 : Bonjour, je suis le prince Tig. Je sais que tu aimerais rencontrer mon père mais avant ça, tu dois résoudre cette énigme : Lors d'une course aux chevaux, si le cavalier double le deuxième, quel est le numéro de sa place ?");
        guessInt = userInput(scanner);
        while (guessInt != ans1) {
            System.out.println("🤴 : Ce n'est pas la bonne réponse, recommence.");
            guessInt = userInput(scanner);
        }
        System.out.println(
                "🤴 : Bravo, c'est la bonne réponse. Quand tu double le deuxième, tu prends sa place donc tu es deuxième. Honnêtement c'étais plutôt facile, maintenant tu peux continuer ta route mais si tu veux rencontrer le roi, tu devras renconter ma soeur, la princesse Nevam, et trouver la réponse à son énigme. Elle est dans la basse-cour, c'est par là-bas. Bonne chance car ça sera moins facile qu'avant.");

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(" ");
            }
            if (i == 0 || i == 4) {
                System.out.println("\\🧑");
            } else
                System.out.println("\\*");
        }
        System.out.println("[Basse-cour]");
    }

    public void enigme2(Scanner scanner) {

        /**Méthode d'une énigme avec un regex, qui nous permet d'entrer que des lettres.*/

        System.out.println(
                "👸 : Bonjour, je suis la princesse Nevam. J'espère que mon frère t'as prévenu, mon énigme ne sera pas facile : Je commence la nuit et je termine le matin. Qui suis-je ?");
        boolean loop = true;
        while (loop) {
            guessChar = scanner.next().toLowerCase();
            if (guessChar.length() > 1 && guessChar.matches(pattern)) {
                System.out.println("👸 : Tu n'es pas sur la bonne piste.");
                continue;
            } else if (guessChar.length() > 1 && !guessChar.matches(pattern)) {
                System.out.println(error);
                continue;
            } else if (guessChar.toLowerCase().charAt(0) != ans2) {
                System.out.println("👸 : Ce n'est pas la bonne réponse, recommence.");
                continue;
            } else
                System.out.println(
                        "👸 : Pas mal du tout ! Effectivement, on retrouve la lettre n au début du premier mot et à la fin du deuxième mot. Tu as réussi à résoudre cette énigme mais celle de ma mère, la reine Xunil, crois- moi que tu auras du mal. Va de ce coté, tu la trouveras sûrement dans la galerie.");
            loop = false;
        }
        for (int i = 5; i >= 1; --i) {
            for (int j = 1; j <= i; ++j) {
              System.out.print(" ");
            }
            if (i==1 || i==5)
            System.out.println("/🧑");
            else System.out.println("/*");
          }
          System.out.println("[Galerie]");
    }

    public void enigme3(Scanner scanner){

        /**Méthode d'une énigme avec un try-catch.*/
        
        System.out.println("👑👸 : Bonjour, je suis la reine Xunil. D'après mes enfants, tu es fort pour résoudre les énigmes. Si tu résouds mon énigme, tu pourras ton chemin vers la grande salle où se trouveras sur son trône, mon époux le roi Avaj : Un père et son fils ont 36 ans à eux deux. Le père a 30 ans de plus que son fils. Quel est l'âge du père ?");
        guessInt = userInput(scanner);
        while (guessInt != ans3) {
            System.out.println("👑👸 : Ce n'est pas la bonne réponse, recommence.");
            guessInt = userInput(scanner);
        }
        System.out.println("👑👸 : Tu es fort en calcul mental. Le père a 33 ans et le fils a 3 ans (33-3=30 et 33+3 = 36). Tu peux aller vers la grande salle, je me demande bien ce qui t'attend.");
        for (int i = 0; i < 3; i++) {
            if(i==0){
                System.out.println("|🧑");
            }
            System.out.println("|*");
        }
        System.out.println("|*͟ ͟*͟ ͟*͟ ͟🧑");
        System.out.println("[Porte de la grande salle]");
    }

    public int userInput(Scanner scanner) {

        /**Try-catch pour éviter les mismatch dans le terminal.*/

        boolean inputOk = false;
        int input = 0;
        while (!inputOk) {
            try {
                input = scanner.nextInt();
                inputOk = true;

            } catch (InputMismatchException e) {
                System.out.println(error);
                scanner.next();
            }
        }
        return input;
    }
}