package co.simplon.promo16;

import java.util.Scanner;

import co.simplon.promo16.oop.Enigma;
import co.simplon.promo16.oop.King;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        King game = new King();
        game.intro(scanner);
        Enigma enigma = new Enigma();
        enigma.enigme1(scanner);
        enigma.enigme2(scanner);
        enigma.enigme3(scanner);
        game.surprise(scanner);
        game.end(scanner);
    }
}