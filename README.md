<h1>Jeu d'énigme.<h1> 

<p>Vous devez trouver et entrer la réponse aux questions dans le terminal pour avancer dans le jeu. Certaines questions ne demandent qu'un seul caractère.<p>
<p>Si la réponse ne convient à la question, vous aurez un message de refus et vous devez redonner une réponse.<p>
<p>Bonne chance.<p>
